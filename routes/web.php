<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','PagesController@index');
Route::get('/about','PagesController@about');
Route::get('/inspiration','PagesController@inspiration');
//Route::get('/login','PagesController@login');
//Route::get('/register','PagesController@register');


/* ++++++++++++++++++++++++ ROUTING PRODUCT +++++++++++++++++++++
 * Middleware => GUEST
 *                  URL         METHOD
 * LIST ROUTE => 1. create =>    GET
 *               2. store  =>    POST
 *               3. edit   =>    GET
 *               4. update =>    POST
 *               5. delete =>    POST/GET
 * */

Route::get('/product',               'ProductController@index');
Route::get('/product/create',        'ProductController@create');
Route::get('/product/description/{id}','ProductController@show');
Route::post('/product/store',        'ProductController@store');
Route::get('/product/edit/{id}',     'ProductController@edit');
Route::post('/product/update',       'ProductController@update');
Route::get('/product/delete/{id}',   'ProductController@destroy');




// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {

    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('cart','CartController');

    // Resources url dan method
    /*  Method          URL
     * 0. index   =>     "/"
     * 1. store   =>     "/store"
     * 2. create  =>     "/create"
     * 3. edit    =>     "/edit"
     * 4. update  =>     "/update"
     * 5. destroy =>     "/destroy"
     * */

    Route::get('users/image/{id}','UserController@image');
    Route::get('users/manage/profile/edit','ProfileController@index');
    Route::post('users/manage/profile/update','ProfileController@update');
    Route::get('users/manage/password/edit','ProfileController@changePassword');
    Route::post('users/manage/password/update','ProfileController@updatePassword');


    Route::get('list-order','CartController@showBooking');
    Route::post('list-order/upload-payment','CartController@uploadPayment');

    Route::get('invoice/{id}','InvoiceController@checkout');
    Route::post('checkout/update','InvoiceController@storeToInvoice');
});

Route::group(['middleware' => 'role:admin'], function () {
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/update-profile', 'UserController@showUpdateProfileForm');
    Route::post('/update-profile', 'UserController@updateProfile');
});

Route::group(['middleware' => 'role:seller'], function () {
    Route::get('notification/event','InvoiceController@newInvoice');
    Route::get('image/invoice/{id}','InvoiceController@getBuktiPembayaran');
    Route::post('invoice/{response}/payment','InvoiceController@responseInvoice');
    Route::get('get/review/{id}','RatingController@index');
    Route::get('review/destroy/{id}','RatingController@destroy');
});

Route::group(['middleware' => 'role:customer'], function () {
    Route::get('wishlist','WishlistController@index');
    Route::post('wishlist/store', 'WishlistController@addToWishlist');
    Route::post('wishlist/delete', 'WishlistController@removeWishlist');
    Route::post('review/store','RatingController@store');
});

Route::group(['prefix' => '/product'], function () {

    Route::group(['prefix' => '/eventorganizer'], function () {
        Route::get('/',                 'Product\EventOrganizerController@index')->name('product-eventorganizer');
        Route::get('/show/{id}',        'Product\EventOrganizerController@show')->name('product-eventorganizer-show');
        Route::get('/edit/{id}',        'Product\EventOrganizerController@edit')->name('product-eventorganizer-edit');
        Route::get('/show/{id}',        'Product\EventOrganizerController@show')->name('product-eventorganizer-show');
    });
    Route::group(['prefix' => '/decoration'], function () {
        Route::get('/',                 'Product\DecorationController@index')->name('product-decoration');
        Route::get('/show/{id}',        'Product\DecorationController@show')->name('product-decoration-show');
        Route::get('/edit/{id}',        'Product\DecorationController@edit')->name('product-decoration-edit');
        Route::get('/show/{id}',        'Product\DecorationController@show')->name('product-decoration-show');
    });
    Route::group(['prefix' => '/building'], function () {
        Route::get('/',                 'Product\BuildingController@index')->name('product-building');
        Route::get('/show/{id}',        'Product\BuildingController@show')->name('product-building-show');
        Route::get('/edit/{id}',        'Product\BuildingController@edit')->name('product-building-edit');
        Route::get('/show/{id}',        'Product\BuildingController@show')->name('product-building-show');
    });
    Route::group(['prefix' => '/merchandise'], function () {
        Route::get('/',                 'Product\MerchandiseController@index')->name('product-merchandise');
        Route::get('/show/{id}',        'Product\MerchandiseController@show')->name('product-merchandise-show');
        Route::get('/edit/{id}',        'Product\MerchandiseController@edit')->name('product-merchandise-edit');
        Route::get('/show/{id}',        'Product\MerchandiseController@show')->name('product-merchandise-show');
    });
    Route::group(['prefix' => '/food'], function () {
        Route::get('/',                 'Product\FoodController@index')->name('product-food');
        Route::get('/show/{id}',        'Product\FoodController@show')->name('product-food-show');
        Route::get('/edit/{id}',        'Product\FoodController@edit')->name('product-food-edit');
        Route::get('/show/{id}',        'Product\FoodController@show')->name('product-food-show');
    });
    Route::get('/image/{id}',       'ProductController@getImage')->name('product-image');
    Route::get('/manage',           'SellerController@manage_product')->name('product-manage');
    Route::get('/create',            'SellerController@create')->name('product.create');
    Route::post('/store',            'SellerController@store')->name('product.store');
    Route::get('/edit/{id}',            'SellerController@edit')->name('product.edit');
    Route::post('/update/{id}',            'SellerController@update')->name('product.update');
    Route::delete('/destroy/{id}',            'SellerController@destroy')->name('product.destroy');
});

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Route::post('/search', 'FilterController@filter');
