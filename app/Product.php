<?php

namespace App;

use Ghanem\Rating\Traits\Ratingable as Rating;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
//use Laravel\Scout\Searchable;

class Product extends Model
{
//    use Searchable;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    use Rating;
    protected $fillable = [
        'ProductName', 'Description', 'Price', 'product_type'
    ];

    public function encodeImage($value, $quality)
    {
        return (string) Image::make($value)->encode('jpg', $quality);
    }
}
