<?php

namespace App\Http\Controllers;

use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class WishlistController extends Controller
{
    public function index()
    {
        $wishlists = Wishlist::where('customer_id',auth()->user()->id)
            ->leftJoin('products','products.id','=','wishlists.product_id')
            ->get();
        return view('pages.wishlist.index', compact('wishlists'));
    }

    public function addToWishlist(Request $request) {
        $isExists = Wishlist::where('product_id',$request->get('id'))->first();
        if(!$isExists) {
            $wishlist = new Wishlist();
            $wishlist->product_id = $request->get('id');
            $wishlist->customer_id = auth()->user()->id;
            $wishlist->save();
            $lastWishlist = Session::get('wishlist');
            Session::put('wishlist', $lastWishlist + 1);
        }else {
            $isExists->product_id = $request->get('id');
            $isExists->customer_id = auth()->user()->id;
            $isExists->save();
        }
        return response()->json('success add to wish list', 200);
    }

    public function removeWishlist(Request $request) {
        $wishlist = Wishlist::find($request->get('id'))->delete();
        $lastWishlist = Session::get('wishlist');
        Session::put('wishlist', $lastWishlist - 1);
        return response()->json('success remove wishlist', 200);
    }
}
