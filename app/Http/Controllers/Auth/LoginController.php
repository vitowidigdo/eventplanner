<?php

namespace App\Http\Controllers\Auth;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Wishlist;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, $user)
    {
        $cart = Cart::where('customer_id',auth()->user()->id)->count(); // Ngambil data Cart berdasarkan user yang sedang login... ->  dihitung totalnya
        $wishlist = Wishlist::where('customer_id',auth()->user()->id)->count(); // ngambil data wishlist berdasarkan user yang sedang login -> dihitung totalnya
        Session::put('cart', $cart);
        Session::put('wishlist',$wishlist);
    }

    public function loggedOut(Request $request)
    {
        Session::flush();
    }


}
