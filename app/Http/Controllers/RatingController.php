<?php

namespace App\Http\Controllers;

use App\Product;
use App\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $product = Product::where('products.id',$id)
            ->select('products.id','products.ProductName','products.Description','products.product_type','users.name','users.contact')
            ->leftJoin('users','users.id','=','owner')
            ->first();
//        dd($product);
        $rating = Rating::where('product_id',$id)
            ->select('ratings.*','users.email')
            ->leftJoin('users','ratings.customer_id','=','users.id')
            ->get();
        return view('pages.seller.feedback',compact('id','product','rating'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::first();
        $post = Post::first();

        $rating = $post->rating([
            'rating' => 5
        ], $user);

        dd($rating);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->get('rating') == 0) {
            return response()->json('Please entry this rating product', 422);
        }

        $rating = Rating::where('customer_id',auth()->user()->id)->where('product_id',$request->get('product_id'))->first();

        if (!$rating) {
            $newRating = new Rating();
            $newRating->product_id = $request->get('product_id');
            $newRating->customer_id = auth()->user()->id;
            $newRating->rating = $request->get('rating');
            $newRating->review = $request->get('comment');
            $newRating->save();
            return response()->json('Successful entry review and rating', 200);
        }
        return response()->json('Already Submit',422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $post)
    {
        $rating = $post->updateRating(1, [
            'rating' => 3
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Rating::find($id)->delete();
        return back()->with('Success');
    }
    public function delete($post)
    {
        $post->deleteRating(1);

    }
    public function average ($post)
    {
        $post->avgRating;
    }
}
