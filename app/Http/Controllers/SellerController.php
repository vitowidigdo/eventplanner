<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class SellerController extends Controller
{
    public function manage_product(){
        $products = Product::where('owner', auth()->user()->id)->get();
        return view('pages.seller.manage-product', compact('products'));
    }

    public function create() {
        return view('pages.seller.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // VALIDASI UNTUK PRODUCT
        $validate = Validator::make($request->all(),[
            'ProductName' => 'required',
            'Description' => 'required',
            'Price'       => 'required',
            'product_type' => 'required',
            'photo' => 'image|max:5000',
        ]);

        if($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        else{
            // COLUMN TABLE = INPUT DATA DARI FORM
            $product = new Product();
            $product->ProductName = $request->get('ProductName');
            $product->Description = $request->get('Description');
            $product->Price = $request->get('Price');
            $product->product_type = $request->get('product_type');
            if($request->hasFile('photo'))
                $product->productImage = $product->encodeImage($request->file('photo'), 75);
            $product->owner = auth()->user()->id;
            $product->save();

            return redirect()->route('product-manage')->with('Products Created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::find($id);
        return view('pages.products.show',compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::find($id);
        return view('pages.seller.edit',compact('products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // VALIDASI UNTUK PRODUCT
        $validate = Validator::make($request->all(),[
            'ProductName' => 'required',
            'Description' => 'required',
            'Price'       => 'required',
            'product_type' => 'required',
            'photo' => 'image|max:5000',
        ]);

        if($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }
        else{
            // COLUMN TABLE = INPUT DATA DARI FORM
            $product = Product::find($id);
            $product->ProductName = $request->get('ProductName');
            $product->Description = $request->get('Description');
            $product->Price = $request->get('Price');
            $product->product_type = $request->get('product_type');
            if($request->hasFile('photo'))
                $product->productImage = $product->encodeImage($request->file('photo'), 75);
            $product->save();

            return redirect()->route('product-manage')->with('Products Updated');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products = Product::find($id);
        $products->delete();
        return redirect()->route('product-manage')->with('Products Remove');
    }
}
