<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        if(auth()->user()) {
            $user = auth()->user();
            if($user->hasRole('admin')) {
                return redirect()->route('users.index');
            }
        }
        return view('pages.index');
    }
    public function about(){
        return view('pages.about');
    }
    public function inspiration() {
        return view('pages.inspiration');
    }
    public function login(){
        return view('pages.login');
    }
    public function Register(){
        return view('pages.register');
    }
    public function Product(){
        return view('pages.products.index');
    }
}
