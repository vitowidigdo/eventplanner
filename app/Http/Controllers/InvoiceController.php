<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Invoice;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function checkout($id)
    {
        $dec_id = Crypt::decrypt($id);
        $query = "select 
                    t1.id,
                    t2.ProductName,
                    t2.Price,
                    t2.Description,
                    t1.event_date as invoice_due_date,
                    t1.created_at as invoice_date,
                    t4.email as owner_email,
                    t4.contact as owner_contact,
                    t3.name as customer_name,
                    t3.email as customer_email
                from invoices as t1
                left join products as t2 on t1.product_id = t2.id
                left join users as t3 on t1.customer_id = t3.id
                left join users as t4 on t1.owner = t4.id
                where t1.id = '$dec_id'";
        $invoice = DB::select(DB::raw($query))[0];
        return view('pages.invoice.checkout',compact('invoice'));
    }

    public function storeToInvoice(Request $request)
    {
        $totalCart = count($request->get('product'));
        for($i = 0;$i < $totalCart; $i++) {
            $newId = strtoupper(substr(uniqid('INV'),0,-1));
            $productOwner = Product::find($request->get('product')[$i]['value']);
            $invoice = new Invoice();
            $invoice->id = $newId;
            $invoice->product_id = $request->get('product')[$i]['value'];
            $invoice->customer_id = auth()->user()->id;
            $invoice->owner = $productOwner->owner;
            $invoice->status = 'not yet paid';
            $invoice->event_date = $request->get('dates')[$i]['value'];
            $invoice->save();
            Cart::where('customer_id',auth()->user()->id)->delete();
        }

        Session::put('cart',0);
        return response()->json('success',200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function newInvoice() {
        $list_invoice = Invoice::where('invoices.owner', auth()->user()->id)
            ->select('invoices.id as invoice_id','event_date','invoices.status','products.*','users.name','users.contact')
            ->leftJoin('products','products.id','=','invoices.product_id')
            ->leftJoin('users','users.id','=','invoices.customer_id')
            ->get();
        return view('pages.invoice.notification-invoice', compact('list_invoice'));
    }

    /**
     * $id => ID PRODUCT
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getBuktiPembayaran($id) {
        $image = Invoice::find($id);
        return response($image->bukti_transfer,200)->header('Content-Type','Image/JPEG'); // Terjadi Decode base64
    }

    public function responseInvoice(Request $request, $response)
    {
        $invoice = Invoice::find($request->get('id'));
        $invoice->status = ($response == 'accept' ? 'Approved' : 'Rejected');
        $invoice->save();

        return response()->json('success',200);
    }
}
