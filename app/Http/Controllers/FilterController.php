<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function filter (Request $request){
            $filters = [
            'product_type'  => Input::get('product_type'),
            'created_at'         => Input::get('created_at'),
            ];

            $user = User::where(function ($query) use ($filters) {
                        if ($filters['product_type']) {
                            $query->where('product_type', '=', $filters['product_type']);
                        }
            if ($filters['created_at']) {
                $query->where('created_at', '=', $filters['created_at']);
            }
            })->get();
            return $user;
    }
    }
