<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class CartController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts = Cart::where('customer_id',auth()->user()->id)
            ->select('products.id as product_id','products.ProductName','products.Description','products.Price','products.owner','users.name','users.contact','users.email','carts.*')
            ->leftJoin('products','products.id','=','carts.product_id')
            ->leftJoin('users','users.id','=','products.owner')
            ->get();
        $totalPrice = 0;
        foreach ($carts as $data) {
            $totalPrice = $totalPrice + $data->Price + (0.1 * $data->Price);
        }
        return view('pages.carts.index', compact('carts', 'totalPrice'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->user() == null) return abort(404); // Kalau session user tidak ada , langsung 404 Page not found

        $isExists = Cart::where('product_id', $request->get('product_id'))
            ->where('customer_id',auth()->user()->id)
            ->first();

        if($isExists) return back()->with('success','Product has Already exists in your cart');

        Cart::create([
            'product_id' => $request->get('product_id'),
            'customer_id' => auth()->user()->id,
        ]);

        $totalCart = Session::get('cart');
        Session::put('cart', $totalCart + 1); // Total cart tambah 1
        return back()->with('success', 'Success add to your cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $rowId, $product)
    {
        Cart::update($rowId, $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::find($id)->delete();
        $cartSession = Session::get('cart');
        Session::put('cart',$cartSession-1); // Total cart sekarang dikurang 1
        return redirect()->route('cart.index')
            ->with('success','Role deleted successfully');
    }

    public function restore(){

        Cart::restore('username');
    }

    public function showBooking() {

        /* ada 2 table yang di join
         *  1. table produk
         *  2. table user
         *
         *  yang diambil dari database =>
         * 1. invoice_id
         * 2. invoice_status
         * 3. semua attribute product (productName, Price, dan lain lain)
         * 4. name dari owner product
         * */
        $list_order = Invoice::where('customer_id', auth()->user()->id)
            ->select('invoices.id as invoice_id','invoices.status','products.*','users.name')
            ->leftJoin('products','products.id','=','invoices.product_id')
            ->leftJoin('users','users.id','=','invoices.owner')
            ->get();
        return view('pages.booking.list_order', compact('list_order'));
    }
    public function uploadPayment(Request $request) {

        $invoice = Invoice::find($request->get('invoice_id'));
        $invoice->status = 'waiting for confirmation';
        $invoice->bukti_transfer = (string) Image::make($request->file('payment_receipt'))->encode('jpg', 75); // gambar di encode jadi base64 dan diubah menjadi string agar bisa dibaca oleh database
        $invoice->save();

//        Invoice::where('id',$request->get('invoice_id'))->update([
//            'status' => 'waiting for confirmation',
//            'bukti_transfer' => (string) Image::make($request->file('payment_receipt'))->encode('jpg', 75),
//        ]);
        return back()->with('success','Success upload payment receipt');
    }
}
