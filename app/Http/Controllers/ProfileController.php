<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index()
    {
        return view('pages.profile.index');
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|unique:users,email,'.auth()->user()->id,
            'gender' => 'required',
            'contact' => 'required',
            'file' => 'image|max:2000'
        ]);

        if($validate->fails()) return back()->withErrors($validate)->withInput();

        $user = User::find(auth()->user()->id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->gender = $request->get('gender');
        $user->contact = $request->get('contact');
        if($request->hasFile('file')) $user->img_profile = $user->encodeImage($request->file('file'), 75);
        $user->save();

        return redirect('/')->with('success','Profile has been updated');
    }

    public function changePassword()
    {
        return view('pages.profile.change-password');
    }

    public function updatePassword(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ]);

        if($validate->fails()) return back()->withErrors($validate)->withInput();

        // Check if Field old Password not match with Current password user
        if(Hash::check($request->get('old_password'), auth()->user()->password) == false)
            return back()->with('error','Old password is invalid');


        // Check if Field old password and new password is same
        if($request->get('old_password') == $request->get('new_password'))
            return back()->with('error','Old password and New Password cant be same');

        $user = User::find(auth()->uset()->id);
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect('/')->with('success','Success update new password');

    }
}
