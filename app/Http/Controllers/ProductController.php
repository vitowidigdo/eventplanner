<?php

namespace App\Http\Controllers;

use App\Product;
use App\Rating;
use http\Env\Response;
use Validator;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
//        $this->middleware('permission:product-list');
        $this->middleware('permission:product-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }

    public function getImage($id)
    {
        $image = Product::find($id);
        return response($image->productImage, 200)->header('Content-Type', 'Image/JPEG');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('pages.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // VALIDASI UNTUK PRODUCT

        $validate = Validator::make($request->all(),[
            'ProductName' => 'required',
            'Description' => 'required',
            'Price'       => 'required',
        ]);

        if($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }
        else{
            // COLUMN TABLE = INPUT DATA DARI FORM
            Product::create($request->all());

            return redirect('/product/create')->with('Products Created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::find($id);
        return view('pages.products.show',compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::find($id);
        return view('pages.products.edit',compact('products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(),[
            'ProductName' => 'required',
            'Description' => 'required',
            'Price'       => 'required',
        ]);

        if($validate->fails())
        {
            return redirect()->back()->withErrors($validate);
        }
        else{
            // COLUMN TABLE = INPUT DATA DARI FORM
            $product = Product::find($id);
            $product->ProductName = $request['ProductName'];
            $product->Description = $request['Description'];
            $product->Price       = $request['Price'];
            $product->save();

            return redirect('/product/edit')->with('Products Updated');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products = Product::find($id);
        $products->delete();
        return redirect('/product/edit')->with('Products Remove');
    }
}
