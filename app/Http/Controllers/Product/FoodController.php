<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Product;
use App\Rating;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
//        $this->middleware('permission:product-list',['only' => ['index','show']]);
        $this->middleware('permission:product-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }

    /**
     * @return mixed
     */
    public function index() {
        if(auth()->guest() || auth()->user()->hasRole('customer')) {
            $product = Product::where('product_type','food')->get();
        }else {
            $product = Product::where('product_type','food')
                ->where('owner', auth()->user()->id)
                ->get();
        }
        return view('pages.products.food.index', compact('product'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id) {
        $product = Product::where('products.id',$id)
            ->select('products.*','users.name','users.contact')
            ->leftJoin('users','users.id','=','owner')
            ->first();
        $rate = null;
        $review = null;
        if (auth()->check()) {
            $rating = Rating::where('customer_id',auth()->user()->id)->where('product_id',$id)->first();
            if ($rating) {
                $rate = $rating->rating;
                $review = $rating->review;
            }else {
                $rate = 0;
                $review = null;
            }
        }
        return view('pages.products.food.show', compact('product','rate','review'));
    }

    /**
     * @return mixed
     */
    public function create() {
        return view('pages.products.food.create');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'product_name' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);
        if($validate->fails())
            return back()->withError($validate)->withInput();

        $product = new Product();
        $product->ProductName = $request->get('product_name');
        $product->Description = $request->get('description');
        $product->Price = $request->get('price');
        $product->productImage = base64_encode($request->get('product_image'));
        $product->product_type = 'food';
        $product->save();

        return redirect()->route('product-food');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id) {
        $product = Product::find($id);
        return view('pages.products.food.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
           'product_name' => 'required',
           'description' => 'required',
           'price' => 'required',
        ]);

        if($validate->fails())
            return back()->withError($validate)->withInput();

        $product = Product::find($id);
        $product->ProductName = $request->get('product_name');
        $product->Description = $request->get('description');
        $product->Price = $request->get('price');
        $product->productImage = base64_encode($request->get('product_image'));
        $product->save();

        return redirect()->route('product-food');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id) {
        $product = Product::find($id)->delete();
        return redirect()->route('product-food');
    }
}
