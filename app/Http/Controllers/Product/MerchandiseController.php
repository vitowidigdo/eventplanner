<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Product;
use App\Rating;
use Illuminate\Http\Request;

class MerchandiseController extends Controller
{
    function __construct()
    {
//        $this->middleware('permission:product-list',['only' => ['index','show']]);
        $this->middleware('permission:product-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }

    /**
     * @return mixed
     */
    public function index() {
        if(auth()->guest() || auth()->user()->hasRole('customer')) {
            $product = Product::where('product_type','Merchandise')->get();
        }else {
            $product = Product::where('product_type','Merchandise')
                ->where('owner', auth()->user()->id)
                ->get();
        }
        return view('pages.products.merchandise.index', compact('product'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id) {
        $product = Product::where('products.id',$id)
            ->select('products.*','users.name','users.contact')
            ->leftJoin('users','users.id','=','owner')
            ->first();
        $rate = null;
        $review = null;
        if (auth()->check()) {
            $rating = Rating::where('customer_id',auth()->user()->id)->where('product_id',$id)->first();
            if ($rating) {
                $rate = $rating->rating;
                $review = $rating->review;
            }else {
                $rate = 0;
                $review = null;
            }
        }
        return view('pages.products.merchandise.show', compact('product','rate','review'));
    }
}
