<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'admin';
        $user->email = 'admin@mail.com';
        $user->gender = 'male';
        $user->password = bcrypt('admin');
        $user->save();

        $user->assignRole('admin');

    }
}
