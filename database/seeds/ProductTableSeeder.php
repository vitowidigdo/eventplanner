<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Product::create([
            'ProductName' => 'IsMaya',
            'Description' => 'With a few decades experience, we will help you with your party!',
            'product_type' => 'Event Organizer',
            'Price' => 'Depends on your requirements',
            'owner' => '2',
        ]);

        \App\Product::create([
            'ProductName' => 'IsMaya',
            'Description' => 'With a few decades experience, we will help you with your party!',
            'product_type' => 'Merchandise',
            'Price' => 'Depends on your requirements',
            'owner' => '2',
        ]);

        \App\Product::create([
            'ProductName' => 'IsMaya',
            'Description' => 'With a few decades experience, we will help you with your party!',
            'product_type' => 'Decoration',
            'Price' => 'Depends on your requirements',
            'owner' => '2',
        ]);

        \App\Product::create([
            'ProductName' => 'IsMaya',
            'Description' => 'With a few decades experience, we will help you with your party!',
            'product_type' => 'Building',
            'Price' => 'Depends on your requirements',
            'owner' => '2',
        ]);

        \App\Product::create([
            'ProductName' => 'IsMaya',
            'Description' => 'With a few decades experience, we will help you with your party!',
            'product_type' => 'Food',
            'Price' => 'Depends on your requirements',
            'owner' => '2',
        ]);
    }
}
