<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = Role::create(['name' => 'admin']);
        $permissions = \Spatie\Permission\Models\Permission::all();
        foreach ($permissions as $permission)
        {
            $role1->givePermissionTo($permission);
            $permission->assignRole($role1);
        }
        $role2 = Role::create(['name' => 'seller']);
        $role2->givePermissionTo(['product-list', 'product-create', 'product-edit', 'product-delete']);
        $role3 = Role::create(['name' => 'customer']);
        $role3->givePermissionTo(['product-list']);

    }
}
