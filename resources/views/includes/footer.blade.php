<footer class="py-5 mt-4">
    <div class="container">
        <div class="row">
            <div class="col-6 col-md">
                <p>&copy; EPlanner 2019</p>
            </div>
            <div class="col-6 col-md">
                <a href="/product">
                <h5>Products</h5>
                </a>
                <ul class="list-unstyled text-small">
                    <li><a class="nav-link p-0" href="/product/event-organizer">Event Organizer</a>
                        <a class="nav-link p-0" href="/product/merchandise">Merchandise</a>
                        <a class="nav-link p-0" href="/product/decoration">Decoration</a>
                        <a class="nav-link p-0" href="/product/building">Building</a>
                        <a class="nav-link p-0" href="/product/food">Food</a>
                </ul>
            </div>
            <div class="col-6">
                <a href="/about">
                    <h5>About</h5>
                </a>
            </div>
        </div>
    </div>
</footer>
