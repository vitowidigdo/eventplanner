<script
    src="https://code.jquery.com/jquery-3.4.1.js"
    integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="{{asset('slick/slick.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="{{asset('js/moment.js')}}"></script>
<script src="{{asset('js/jquery.daterangepicker.min.js')}}"></script>
<script src="{{asset('js/starrr.js')}}"></script>
<script src="{{asset('js/jquery.star-rating-svg.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script>
    var rating = 0;

    $(document).ready(function() {
        $('#home-banner').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
        });
        $(".stars").starRating({
            starSize: 25,
            initialRating : $(this).data('rating'),
            useFullStars: true,
            callback: function(currentRating, $el){
                // alert(currentRating);
                rating = currentRating;
            }
        });

    });

    function storeReview(e) {
        // e.preventDefault();
        $.ajax({
            url : '{{url('review/store')}}',
            method : 'post',
            data : {
                _token : '{{csrf_token()}}',
                rating : rating,
                comment : $('textarea[name="comment"]').val(),
                product_id : e,
            },
            success : function (response) {
                alert(response);
            },
            error: function (data) {
                var error = data.responseJSON;
                alert(error);
                console.log(error);
            }
        });
    }

    function addToWishlist(id)
    {
        $.ajax({
            url : '{{url('wishlist/store')}}',
            method : 'post',
            data : {
                _token : '{{csrf_token()}}',
                id : id
            },
            success: function (response) {
                window.location.reload();
            },
        });
    }


</script>

@yield('script')
