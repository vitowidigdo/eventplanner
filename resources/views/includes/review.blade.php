<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5>Rating & Review</h5>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-start">
                        <h4>Rate : </h4>
                        <div class="stars" data-rating="{{$rate}}"></div>
                    </div>

                    <div class="form-group">
                        <label for="">Feedback</label>
                        <textarea name="comment" class="form-control" id="" placeholder="Feedback.." cols="30" rows="10" required></textarea>
                    </div>
                    <button class="btn btn-primary mx-auto" onclick="storeReview({{$product->id}});">submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
