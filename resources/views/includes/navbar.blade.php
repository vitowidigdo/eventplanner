<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
    <a href="{{url('/')}}" class="navbar-brand">
        <img src="{{asset('images/logo-color.png')}}" width="40" alt="">
        {{--{{ config('app.name', 'Event Planner') }}--}}
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{url('/')}}">Home <span class="sr-only"></span></a>
            </li>
            @if(auth()->user() == null ||auth()->user()->hasRole('customer'))
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/product/eventorganizer')}}">Event Organizer <span class="sr-only"></span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/product/merchandise')}}">Merchandise <span class="sr-only"></span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/product/decoration')}}">Decoration <span class="sr-only"></span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/product/building')}}">Building <span class="sr-only"></span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('product-food')}}">Food <span class="sr-only"></span></a>
                </li>
            @else
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('product-manage')}}">Manage Product <span class="sr-only"></span></a>
                </li>
            @endif
            <li class="nav-item active border-left">
                <a class="nav-link" href="{{url('/about')}}">About</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{url('/inspiration')}}">Inspiration</a>
            </li>
        </ul>


        @if(Auth::guest())
            <div>
                <button type="button" class="btn btn-outline-light dropdown-toggle ml-2"  id="signInDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sign In</button>
                <div class="dropdown-menu" style="float: right !important; right: 0 !important; left: auto" aria-labelledby="signInDropdown">
                    <form method="POST" action="{{ route('login') }}" class="px-4 py-3">
                        @csrf

                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" placeholder="E-mail Address" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary">Sign in</button>
                    </form>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{url('/register')}}">New around here? Sign up</a>
                    <a class="dropdown-item" href="{{url('/password/reset')}}">Forgot password?</a>
                </div>
            </div>
        @else
            @if(auth()->user()->hasRole('customer'))
                <a href="{{url('wishlist')}}" class="btn btn-outline-light text-white mr-2"><i class="far fa-heart"></i> <span id="totalCart" class="badge badge-light">{{Session::get('wishlist')}}</span></a>
                <a href="{{url('cart')}}" class="btn btn-outline-light text-white"><i class="fas fa-shopping-cart"></i> <span id="totalCart" class="badge badge-light">{{Session::get('cart')}}</span></a>
            @endif
            <div class="dropdown">
                <a class="btn border-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{url('users/image/'.auth()->user()->id)}}" width="35" style="border-radius: 100px;">
                </a>

                <div class="dropdown-menu dropdown-menu-right mt-2 rounded-0 border-0 shadow mb-5 bg-white rounded" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="{{url('users/manage/profile/edit')}}">Manage Profile</a>
                    <a class="dropdown-item" href="{{url('users/manage/password/edit')}}">Change Password</a>
                    @if(auth()->user()->hasRole('customer'))
                        <a class="dropdown-item" href="{{url('list-order')}}">Booking</a>
                    @else
                        <a class="dropdown-item" href="{{url('notification/event')}}">Notification</a>
                    @endif
                    <div class="dropdown-divider"></div>
                    <a href="" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Logout</a>
                    <form action="{{route('logout')}}" method="post" id="logout-form">
                    @csrf
                    </form>
                </div>
            </div>

        @endif

    </div>
</nav>
