@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-light">
                        <h1 class="display-4">Update Event</h1>
                    </div>
                    <div class="card-body">
                        <form action="{{route('product.update',['id' => $products->id])}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="">Event Name</label>
                                <input type="text" name="ProductName" class="form-control @error('ProductName') is-invalid @enderror" value="{{$products->ProductName}}">
                                <div class="invalid-feedback">
                                    @error('ProductName')
                                    <span>{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea name="Description" id="" rows="10" class="form-control @error('Description ') is-invalid @enderror">{{$products->Description}}</textarea>
                                <div class="invalid-feedback">
                                    @error('Description')
                                    <span>{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Price</label>
                                <input type="number" name="Price" class="form-control @error('Price') is-invalid @enderror" value="{{$products->Price}}">
                                <div class="invalid-feedback">
                                    @error('Price')
                                    <span>{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Image</label>
                                <input type="file" name="photo" class="form-control pt-1 @error('photo') is-invalid @enderror">
                                <div class="invalid-feedback">
                                    @error('photo')
                                    <span>{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Event Type</label>
                                <select class="custom-select @error('product_type') is-invalid @enderror" name="product_type" value="{{$products->product_type}}" multiple>
                                    <option value="Event Organizer" {{($products->product_type == 'Event Organizer' ? 'selected' : '')}}>Event Organizer</option>
                                    <option value="Merchandise" {{($products->product_type == 'Merchandise' ? 'selected' : '')}}>Merchandise</option>
                                    <option value="Decoration" {{($products->product_type == 'Decoration' ? 'selected' : '')}}>Decoration</option>
                                    <option value="Building" {{($products->product_type == 'Building' ? 'selected' : '')}}>Building</option>
                                    <option value="Food" {{($products->product_type == 'Food' ? 'selected' : '')}}>Food</option>
                                </select>
                                <div class="invalid-feedback">
                                    @error('product_type')
                                    <span>{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <button class="btn btn-primary">Save Changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
