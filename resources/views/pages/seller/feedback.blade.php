@extends('layouts.app')

@section('content')
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card">
                    <div class="card-header">
                        <h3>{{$product->ProductName}}</h3>
                    </div>
                    <img src="{{route('product-image',['id'=>$id])}}" class="img-fluid w-100" alt="">
                    <div class="card-body">
                        <p>{{$product->Description}}</p>
                        <div class="d-flex justify-content-start border p-3">
                            <div class="col-sm-4"><i class="fas fa-user-tie"></i> {{$product->name}}</div>
                            <div class="col-sm-4"><i class="fab fa-whatsapp"></i> {{$product->contact}}</div>
                        </div>
                        <p class="text-muted">If you want to ask about this event, please contact the owner of the event listed above</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 border-left">
                <div class="card">
                    <div class="card-header">
                        <h3>Rating & Review</h3>
                    </div>
                    <div class="card-body">
                        <h5>Total Feedback : {{count($rating)}}</h5>
                        <hr>
                        @if(count($rating) == 0)
                            <h4 class="text-center">No rating & review</h4>
                        @else
                            <ul class="list-group">
                                @foreach($rating as $data)
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        {{($data->review == null ? 'No Review' : $data->review)}}
                                        <span class="badge badge-primary badge-pill" style="position: absolute; top: 15px; right: 10px;"> Rate : {{$data->rating}}</span>
                                        <a href="{{url('review/destroy/'.$data->id)}}" class="text-danger" style="position:absolute; top:-10px; right: -5px;"><i class="fas fa-trash"></i></a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
