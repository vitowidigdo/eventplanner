@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb mb-2">
            <div class="pull-left">
                <h2>Products</h2>
            </div>
            <div class="pull-right">
                @can('product-create')
                    <a class="btn btn-success rounded-0 btn-sm" href="{{ route('product.create') }}"> Create New Product</a>
                @endcan
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-sm table-bordered" id="table_product">
        <thead>
        <tr>
            <th>No</th>
            <th>Event Name</th>
            <th>Description</th>
            <th>Product Type</th>
            <th width="280px">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($products as $i => $product)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $product->ProductName }}</td>
                <td class="text-truncate">{{ $product->Description }}</td>
                <td>{{$product->product_type}}</td>
                <td>
                    <form action="{{ route('product.destroy',$product->id) }}" method="POST">
                        <a class="btn btn-info btn-sm rounded-0" href="{{ route('product-'.strtolower(str_replace(' ','',$product->product_type)).'-show',$product->id) }}">Show</a>
                        @can('product-edit')
                            <a class="btn btn-sm rounded-0 btn-primary" href="{{ route('product.edit',$product->id) }}">Edit</a>
                        @endcan


                        @csrf
                        @method('DELETE')
                        @can('product-delete')
                            <button type="submit" class="btn btn-sm rounded-0 btn-danger">Delete</button>
                        @endcan
                        <a href="{{url('get/review/'.$product->id)}}" class="btn btn-sm rounded-0 btn-info">Rating & Review</a>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    {{--{!! $products->links() !!}--}}


@endsection

@section('script')
    <script>
        $('#table_product').DataTable();
    </script>
@endsection
