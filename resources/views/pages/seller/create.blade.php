@extends('layouts.app')

@section('content')
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <h1 class="display-4">Create New Event</h1>
                        </div>
                        <div class="card-body">
                            <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="">Event Name</label>
                                    <input type="text" name="ProductName" class="form-control @error('ProductName') is-invalid @enderror" value="{{old('ProductName')}}">
                                    <div class="invalid-feedback">
                                        @error('ProductName')
                                            <span>{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="Description" id="" rows="10" class="form-control @error('Description ') is-invalid @enderror">{{old('Description')}}</textarea>
                                    <div class="invalid-feedback">
                                        @error('Description')
                                        <span>{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Price</label>
                                    <input type="number" name="Price" class="form-control @error('Price') is-invalid @enderror" value="{{old('Price')}}">
                                    <div class="invalid-feedback">
                                        @error('Price')
                                        <span>{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Image</label>
                                    <input type="file" name="photo" class="form-control pt-1 @error('photo') is-invalid @enderror">
                                    <div class="invalid-feedback">
                                        @error('photo')
                                        <span>{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Event Type</label>
                                    <select class="custom-select @error('product_type') is-invalid @enderror" name="product_type" value="{{old('product_type')}}" multiple>
                                        <option value="Event Organizer">Event Organizer</option>
                                        <option value="Merchandise">Merchandise</option>
                                        <option value="Decoration">Decoration</option>
                                        <option value="Building">Building</option>
                                        <option value="Food">Food</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        @error('product_type')
                                        <span>{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <button class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
