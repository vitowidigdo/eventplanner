@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card mt-5">
                    <div class="card-header">
                        <h3>List Order</h3>
                    </div>
                    <div class="card-body">
                        <div class="list-group">
                            @foreach($list_order  as $index => $data)
                                <div class="list-group-item list-group-item-action">
                                    <div class="row">
                                        <img src="{{asset('images/restaurant.jpg')}}" class="col-lg-3" width="250" alt="">
                                        <div class="col-lg-9">
                                            <div class="d-flex justify-content-between">
                                                <h5 class="mb-1">{{$data->ProductName}}</h5>
                                                <small>{{\Carbon\Carbon::createFromTimeString($data->created_at)->toDayDateTimeString()}}</small>
                                            </div>
                                            <p class="mb-1">{{$data->Description}}</p>
                                            <div class="">
                                                <small><i class="fas fa-user-tie"></i> {{$data->name}}</small>
                                                <small class="ml-3"><i class="fab fa-whatsapp-square"></i> {{($data->contact != null ? $data->contact : 'No Contact Available')}}</small>
                                                <a class="btn btn-link p-0" href="{{url('invoice/'.\Illuminate\Support\Facades\Crypt::encrypt($data->invoice_id))}}">see detail</a>
                                                @if($data->status == 'not yet paid')
                                                    <button class="btn btn-sm btn-primary float-right" onclick="openModal('{{$data->invoice_id}}')">Upload payment receipt</button>
                                                @elseif($data->status = 'waiting for confirmation')
                                                    <h6 class="text-info float-right">Waiting for Confirmation</h6>
                                                @else
                                                    <small class="text-success float-right">Approved</small>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalUploadPayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upload payment receipt</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-upload-payment" action="{{url('list-order/upload-payment')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="invoice_id">
                        <input type="file" name="payment_receipt" accept="image/*">
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" onclick="$('#form-upload-payment').submit();">Upload</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function openModal(id) {
            $('input[name="invoice_id"]').val(id);
            $('#modalUploadPayment').modal('show');
        }
    </script>
@endsection
