@extends('layouts.app')
@section('content')
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card-columns">
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-1.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-2.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-3.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-4.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-5.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-6.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-7.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-8.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-9.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-10.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-11.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-12.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-13.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-14.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-15.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-16.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-17.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card">
                        <img src="{{asset('images/inspiration/pict-18.jpg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <footer class="blockquote-footer text-white">
                                    <small>
                                        Someone famous in <cite title="Source Title">Source Title</cite>
                                    </small>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
