@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center align-content-center">
            <div class="col-lg-3 d-flex align-content-center">
                <h1 class="display-1 align-self-center">HOME</h1>
            </div>
            <div class="col-lg-9">
                <div id="home-banner">
                    <div>
                        <img src="{{url('/images/banner-1.jpg')}}" height="400" alt="">
                    </div>
                    <div>
                        <img src="{{url('/images/banner-2.jpg')}}" height="400" alt="">
                    </div>
                    <div>
                        <img src="{{url('/images/banner-3.jpg')}}" height="400" alt="">
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="{{asset('/images/concert-336695_1280.jpg')}}" class="card-img h-100" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Event Organizer</h5>
                                <p class="card-text">Want to rent some Event Organizer for your Event?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-sm-12">
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="{{asset('/images/waffle-heart-2697904_1280.jpg')}}" class="card-img h-100" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Merchandise</h5>
                                <p class="card-text">Want to make some Merchandise for your Event?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-sm-12">
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="{{asset('/images/restaurant.jpg')}}" class="card-img h-100" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Decoration</h5>
                                <p class="card-text">Make the best decoration for your event!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-sm-12">
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="{{asset('/images/african-food.jpg')}}" class="card-img h-100" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Food</h5>
                                <p class="card-text">Choose the Best Food for your event!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-sm-12">
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="{{asset('/images/florence-1066314_1280.jpg')}}" class="card-img h-100" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Rent a building</h5>
                                <p class="card-text">Rent the best building on yout choice!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
