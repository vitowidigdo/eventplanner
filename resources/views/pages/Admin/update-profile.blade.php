@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Update Profile</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{url('update-profile')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{auth()->user()->name}}" required/>
        </div>
        <div class="form-group">
            <label for="">email</label>
            <input type="email" class="form-control" name="email" value="{{auth()->user()->email}}" readonly required>
        </div>
        <div class="form-group">
            <label for="">Contact</label>
            <input type="text" class="form-control" name="contact" value="{{auth()->user()->contact}}" maxlength="12" required>
        </div>
        <div class="form-group">
            <label for="">Gender</label>
            <div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="male" {{(auth()->user()->gender == 'male') ? 'checked' : ''}} value="male">
                    <label class="form-check-label" for="male">Male</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="female" {{(auth()->user()->gender != 'male') ? 'checked' : ''}} value="female">
                    <label class="form-check-label" for="female">Female</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Address</label>
            <textarea name="address" id="" cols="30" rows="10" class="form-control" required>{{auth()->user()->Address}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Save Changes</button>
    </form>
@endsection
