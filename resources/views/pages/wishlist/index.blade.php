@extends('layouts.app')
@section('content')
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Wishlist</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach($wishlists as $data)
                                <div class="col-lg-6 col-sm-12 mb-3">
                                    <a href="{{route('product-food-show',['id' => $data->product_id])}}" class="card nav-link text-body" style="max-width: 540px;">
                                        <div class="row no-gutters">
                                            <div class="col-md-4">
                                                <img src="{{route('product-image',['id'=>$data->product_id])}}" class="card-img h-100" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card-body">
                                                    <h5 class="card-title">{{$data->ProductName}}</h5>
                                                    <p class="pl-0 col-12 text-truncate">{{$data->Description}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <button type="button" onclick="destroyWishlist({{$data->wishlist_id}})" class="btn btn-link float-right text-danger position-absolute" style="top: 0; right: 20px;"><i class="fas fa-times"></i></button>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function destroyWishlist(id){
            $.ajax({
                url : '{{url('wishlist/delete')}}',
                method : 'post',
                data : {
                    _token : '{{csrf_token()}}',
                    id : id,
                },
                success: function () {
                    window.location.reload();
                }
            });
        }
    </script>
@endsection
