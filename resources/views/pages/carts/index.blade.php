@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Carts</h1>
        @if(Session::get('cart') == 0)
            <div class="row justify-content-center align-items-center" style="height: 50vh;">
                <div class="col-12 text-center">
                    <h1 class="display-4">you haven't added to cart</h1>
                </div>
                <a href="{{url('/')}}" class="btn btn-outline-dark">CONTINUE SHOP</a>
            </div>
        @else
            <div class="row justify-content-center border">
                <div class="col-lg-8">
                    @foreach($carts as $data)
                        <div class="card border-0 rounded-0 mt-2">
                            <div class="card-header text-center d-flex justify-content-between ">
                                <h4>{{$data->ProductName}}</h4>
                                <div class="d-flex justify-content-between">
                                    <div class="border d-flex mr-2">
                                        <input type="hidden" name="product_id[]" value="{{$data->product_id}}">
                                        <input type="text" name="event_date[]" class="form-control-plaintext pl-2" placeholder="Select Date" value="">
                                        <button class="btn openDate"><i class="far fa-calendar-alt"></i></button>
                                    </div>

                                    <form action="{{ route('cart.destroy',$data->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>
                                    </form>
                                </div>
                            </div>
                            <img src="{{asset('images/food-image-header.jpg')}}" alt="" class="card-img-top rounded-0">
                            <div class="card-body">
                                <p class="text-muted">Description</p>
                                {{$data->Description}}
                                <div class="d-flex justify-content-between border p-2">
                                    <span><i class="fas fa-user-tie"></i> {{$data->name}}</span>
                                    <span><i class="far fa-envelope"></i> {{$data->email}}</span>
                                    <span><i class="fab fa-whatsapp"></i> {{$data->contact}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-lg-4">
                    <div class="card border-0 mt-2">
                        <div class="card-header text-center">
                            <h4>{{config('app.name')}} Cart</h4>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                @foreach($carts as $data)
                                    <tr>
                                        <td>{{$data->ProductName}}</td>
                                        <th>Rp. {{number_format($data->Price)}}</th>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td>PPN</td>
                                    <th>10%</th>
                                </tr>
                                <tr class="bg-secondary text-white">
                                    <td>Total</td>
                                    <th>Rp. {{number_format($totalPrice)}}</th>
                                </tr>
                            </table>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-outline-dark btn-block" onclick="checkout(this)">CHECKOUT</button>
                            <a href="{{url('/')}}" class="btn btn-outline-dark btn-block">CONTINUE SHOP</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
@section('script')
    <script>
        $('input[name="event_date[]"]').each(function () {
            $(this).dateRangePicker({
                autoClose: true,
                singleDate : true,
                showShortcuts: false,
                singleMonth: true
            });
        });

        $('.openDate').each(function () {
            $(this).click(function(evt) {
                evt.stopPropagation();
                var input = $(this).siblings('input');
                $(input).data('dateRangePicker').open();
            });
        });

        function checkout(e) {
            // e.preventDefault();
            var validate_date = true;
            var dates = $(':input[name="event_date[]"]').serializeArray();
            var product_id = $(':input[name="product_id[]"]').serializeArray();

            console.log(product_id);
            dates.forEach(function (key, index) {
                if(key.value === "")
                    validate_date = false;
            });

            if(!validate_date)
                alert('Please pick your date each of your event!');

            else {
                $.ajax({
                    url : '{{url('checkout/update')}}',
                    method : 'post',
                    data : {
                        _token : $('meta[name="csrf-token"]').attr('content'),
                        dates : $(':input[name="event_date[]"]').serializeArray(),
                        product : $(':input[name="product_id[]"]').serializeArray(),
                    },
                    success: function (response) {
                        alert('Success checkout, please make payment immediately, thank you!');
                        window.location.reload();
                    }
                })
            }

        }
    </script>
@endsection
