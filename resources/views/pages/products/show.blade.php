@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <img src="{{asset('/images/banner-1.jpg')}}" class="img-fluid" alt="">
            </div>
            <div class="col-lg-8">
                <h1>{{$products->ProductName}}</h1>
                <div>
                    {{$products->Description}}
                    <p>Price : {{$products->Price}}</p>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('users.index') }}"> Book and plan your events with us!</a>
                </div>
            </div>
        </div>
    </div>
@endsection