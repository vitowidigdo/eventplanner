@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        @if(session('success'))
        <!-- Modal -->
            <div class="modal fade" id="session" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body text-center">
                            <h4>{{session('success')}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <h4 class="display-4 text-center">{{$product->ProductName}}</h4>
                <img src="{{route('product-image',['id'=>$product->id])}}" class="img-fluid w-100" alt="">
                <div class="row mt-4">
                    <div class="col-sm-8 border-right">
                        <p>{{$product->Description}}</p>
                        <div class="d-flex justify-content-start border p-3">
                            <div class="col-sm-4"><i class="fas fa-user-tie"></i> {{$product->name}}</div>
                            <div class="col-sm-4"><i class="fab fa-whatsapp"></i> {{$product->contact}}</div>
                        </div>
                        <p class="text-muted">If you want to ask about this event, please contact the owner of the event listed above</p>
                        @if(auth()->user() != null && auth()->user()->hasRole('customer'))
                            @include('includes.review')
                        @endif
                    </div>

                    <div class="col-sm-4">
                        <h5>Price</h5>
                        <h3>{{(is_numeric($product->Price) ? 'Rp. '.number_format($product->Price) : $product->Price)}}</h3>
                        @role('seller')
                        <a href="{{route('product-manage')}}" class="btn btn-block btn-primary">Back</a>
                        @else
                            @guest()
                                <a href="{{route('login')}}" class="btn btn-primary btn-block rounded-0"><i class="fas fa-sign-in-alt"></i> Login </a>
                            @else
                                <form action="{{route('cart.store')}}" method="POST">
                                    @csrf
                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                    <button type="button" class="btn btn-light" id="wishlistBtn" onclick="addToWishlist({{$product->id}})"><i class="far fa-heart fa-2x"></i></button>
                                    <button class="btn btn-primary btn-block rounded-0"><i class="fas fa-cart-plus"></i> Add To Cart</button>
                                </form>
                            @endguest
                        @endrole
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @if(session('success'))
        <script>
            $('#session').modal('show');
        </script>
    @endif
@endsection
