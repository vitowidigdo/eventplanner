@extends('layouts.app')
@section('content')
    <div class="card rounded-0 border-0">
        <div class="card-header text-center"><h1>Decoration</h1></div>
        <img src="{{url('images/decoration-image-header.jpg')}}" class="card-img-top img-fluid" alt="">
        <div class="card-body row justify-content-center">
            @foreach($product as $data)
                <div class="col-lg-6 col-sm-12">
                    <a href="{{route('product-decoration-show',['id' => $data->id])}}" class="card nav-link text-body" style="max-width: 540px;">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="{{route('product-image',['id'=>$data->id])}}" class="card-img h-100" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">{{$data->ProductName}}</h5>
                                    <p class="pl-0 col-12 text-truncate">{{$data->Description}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
