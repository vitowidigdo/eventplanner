@extends('layouts.app')

@section('content')
    <h1>Edit Product</h1>
    <form method="POST" action="{{url('/product/edit')}}">
        {{csrf_field()}}
        <div class="form-group">
            Product name: <input type="text" name="ProductName">
            @if($errors->has('ProductName'))
                <span>{{$errors->first('ProductName')}}</span>
            @endif
            <br>
            Description: <input type="textArea" name="Description">
            @if($errors->has('Description'))
                <span>{{$errors->first('Description')}}</span>
            @endif
            <br>
            Price :<input type="number" name="Price">
            @if($errors->has('Price'))
                <span>{{$errors->first('Price')}}</span>
            @endif
            <br>
        </div>
        <input type="submit" value="Submit">
    </form>
@endsection