@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @foreach($products as $data)
                <div class="col-lg-3 col-md-4 mb-3 ">
                    <a href="{{url('/product/description').'/'.$data->id}}" class="card nav-link p-0 text-dark">
                        <img src="{{asset('images/concert-336695_1280.jpg')}}" class="card-img-top w-50 mx-auto" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{$data->ProductName}}</h5>
                            <p class="card-text">{{$data->Description}}</p>
                        </div>
                        <div class="card-footer d-flex justify-content-around">
                            <span>{{$data->Price}}</span>
                        </div>
                    </a>
                </div>

            @endforeach
        </div>
    </div>
@endsection