@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card mt-5">
                    <div class="card-header">
                        <h3>List Order</h3>
                    </div>
                    <div class="card-body">
                        <div class="list-group">
                            @if(count($list_invoice) != 0)
                                @foreach($list_invoice  as $index => $data)
                                    <a href="#" class="list-group-item list-group-item-action">
                                        <div class="row">
                                            <img src="{{asset('images/restaurant.jpg')}}" class="col-lg-3" width="250" alt="">
                                            <div class="col-lg-9">
                                                <div class="d-flex justify-content-between">
                                                    <h5 class="mb-1">{{$data->ProductName}}</h5>
                                                    <h5>Event Date : {{\Carbon\Carbon::createFromDate($data->event_date)->format('d F Y')}}</h5>
                                                </div>
                                                <p class="mb-1">{{$data->Description}}</p>
                                                <div class="">
                                                    @if($data->status == 'waiting for confirmation')
                                                        <button class="btn btn-sm btn-primary float-right" onclick="openModal('{{$data->invoice_id}}')">Check payment receipt</button>
                                                    @else
                                                        @if($data->status == 'Approved')
                                                            <small class="text-success float-right"><i class="fas fa-check-circle"></i> {{$data->status}}</small>
                                                        @elseif($data->status == 'Rejected')
                                                            <small class="text-danger float-right"><i class="fas fa-times-circle"></i> {{$data->status}}</small>
                                                        @else
                                                            <small class="text-info float-right"> {{$data->status}}</small>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-xl" id="modalCheckPaymentReceipt" abindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="" id="gambar_bukti_pembayaran" class="img-fluid" alt="">
                </div>
                <div class="modal-footer">
                    <button onclick="rejectPayment();" class="btn btn-sm btn-danger">Reject payment receipt</button>
                    <button onclick="acceptPayment();" class="btn btn-sm btn-primary">Accept payment receipt</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var invoice_id = '';
        function openModal(id) {
            invoice_id = id;
            document.getElementById('gambar_bukti_pembayaran').src = '{{url('/image/invoice/')}}/' + id;
            $('#modalCheckPaymentReceipt').modal('show');
        }

        function acceptPayment() {
            $.ajax({
                url : '{{url('/invoice/accept/payment')}}',
                method : 'post',
                data : {
                    _token : '{{csrf_token()}}',
                    id : invoice_id,
                },
                success: function (response) {
                    window.location.reload();
                }
            });
        }
        
        function rejectPayment() {
            $.ajax({
                url : '{{url('/invoice/reject/payment')}}',
                method : 'post',
                data : {
                    _token : '{{csrf_token()}}',
                    id : invoice_id,
                },
                success: function (response) {
                    window.location.reload();
                }
            });
        }
    </script>
@endsection
