@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <div class="card rounded-0">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div>
                            <img src="{{asset('images/logo-color.png')}}" width="150" alt="">
                            <h4>Event Planner</h4>
                            <p class="m-0">Phone : {{$invoice->owner_contact}}</p>
                            <p class="m-0">Email Seller : {{$invoice->owner_email}}</p>
                            <p class="m-0">www.event-planner.com</p>
                        </div>
                        <div>
                            <h2 class="text-muted">INVOICE</h2>
                            <table>
                                <tr>
                                    <td>Invoice #</td>
                                    <td>: {{$invoice->id}}</td>
                                </tr>
                                <tr>
                                    <td>Invoice Date </td>
                                    <td>: {{\Carbon\Carbon::createFromDate($invoice->invoice_date)->format('D M Y')}}</td>
                                </tr>
                                <tr>
                                    <td>Due date :</td>
                                    <td>: {{\Carbon\Carbon::createFromDate($invoice->invoice_due_date)->format('D M Y')}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5>Bill To: </h5>
                    <p class="m-0">{{strtoupper($invoice->customer_name)}}</p>
                    <p class="m-0 font-weight-bold">{{$invoice->customer_email}}</p>

                    {{--Table--}}
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th>Date</th>
                            <th>Event</th>
                            <th>Description</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{\Carbon\Carbon::createFromDate($invoice->invoice_due_date)->format('D M Y')}}</td>
                            <td>{{$invoice->ProductName}}</td>
                            <td>{{$invoice->Description}}</td>
                            <td>Rp. {{number_format($invoice->Price)}}</td>
                        </tr>
                        </tbody>
                        <tfoot class="thead-dark">
                        <tr>
                            <th></th>
                            <th colspan="2" class="text-right">Total</th>
                            <th>Rp. {{number_format($invoice->Price)}}</th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
                <div class="card-footer">
                    <div class="row justify-content-between">
                        <div class="col-6">
                            <h5>Notes</h5>
                            <p>Thank you for your business</p>
                        </div>
                        <div class="col-6">
                            <p>Please pay before the due date, if the payment has not been made for a specified time, this invoice will be canceled</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
