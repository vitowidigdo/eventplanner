@extends('layouts.app')

@section('content')
    <div class="jumbotron">
    <title>E-Planner</title>
    <h4>Welcome to eplanner</h4>
    <p>eventPlanner is the place where you plan and construct the ideal event that you want. Starting from a birthday party,
        team bonding, even wedding, you can plan it with us! If you have any inquiries hit us up at
        <br> xxxxxx@xxxx.xxx
        <br>
        Or with phone at down below
        <br>
        081xxxxxxxxx
        <br>
        </p>
    </div>
@endsection
