@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <div class="card mt-3">
                    <div class="card-header">
                        <h4 class="display-4">Update Profile</h4>
                    </div>
                    <div class="card-body">
                        <form id="form-update-profile" action="{{url('users/manage/profile/update')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row justify-content-center">
                                <img src="{{url('users/image/'.auth()->user()->id)}}" width="150" alt="">
                            </div>
                            <div class="row justify-content-center m-3">
                                <div class="custom-file col-4">
                                    <input type="file" name="file" class="custom-file-input @error('img_profile') is-invalid @enderror" id="customFile" accept="image/*">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{auth()->user()->name}}">
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Name" value="{{auth()->user()->email}}">
                                    @error('email')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Gender</label>
                                <div class="col-sm-10 pt-2">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="gender" class="custom-control-input" value="male" {{(auth()->user()->gender == 'male' ? 'checked' : null)}}>
                                        <label class="custom-control-label" for="customRadioInline1">Male</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="gender" class="custom-control-input" value="female" {{(auth()->user()->gender == 'female' ? 'checked' : null)}}>
                                        <label class="custom-control-label" for="customRadioInline2">Female</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Contact</label>
                                <div class="col-sm-10">
                                    <input type="text" name="contact" class="form-control @error('contact') is-invalid @enderror" placeholder="Contact" value="{{auth()->user()->contact}}">
                                    @error('contact')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                    <textarea name="address" rows="5" class="form-control @error('address') is-invalid @enderror" placeholder="Address"> {{auth()->user()->address}}</textarea>
                                    @error('address')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="{{url('/')}}" class="btn btn-outline-secondary rounded-0 mr-2">Back</a>
                        <button class="btn btn-outline-primary rounded-0" onclick="$('#form-update-profile').submit()">Save Changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
