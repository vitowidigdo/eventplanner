<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('includes.head')
</head>
<body>
@include('includes.navbar')

<div class="container mt-5 pt-3">
    @yield('content')
</div>

@guest()

@else
    {{--@include('includes.footer')--}}
@endguest
@include('includes.script')
</body>
</html>
